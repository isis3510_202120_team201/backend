const express = require("express");
const mongoSanitize = require("express-mongo-sanitize");
const xss = require("xss-clean");
const cors = require("cors");

const testRouter = require("./routes/startupRoutes");
const inversionRouter = require("./routes/inversionRoutes"); // inversion on each industry
const trendingRouter = require("./routes/trendingRoutes"); // inversion on each industry by country
const countryRouter = require("./routes/countriesRoutes"); // list countries
const dashboardRouter = require("./routes/dashboardRoutes"); // list dashboard functions
const twofactorstatsRouter = require("./routes/twofactorstatsRouter"); // list twofactor stats functions
const profileviewsRouter = require("./routes/profileviewsRouter"); // list profileviews functions
const userRolesRoutes = require("./routes/userRolesRoutes"); // list roles functions
const stripeRoute = require("./routes/stripeRoute"); //Stripe route for payments

const app = express();

app.use(cors());
app.use(express.json());
app.use(mongoSanitize());
app.use(xss());

app.get("/", (req, res) => {
  res.send("Welcome to Unicorn API :)");
});

app.use("/api/startups", testRouter);
app.use("/api/inversion", inversionRouter);
app.use("/api/trending", trendingRouter);
app.use("/api/country", countryRouter);
app.use("/api/dashboard", dashboardRouter);
app.use("/api/twofactorstats", twofactorstatsRouter);
app.use("/api/profileviews", profileviewsRouter);
app.use("/api/roles", userRolesRoutes);
app.use("/api/stripe", stripeRoute);

module.exports = app;
