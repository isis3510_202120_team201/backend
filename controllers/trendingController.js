const { MongoClient } = require("mongodb");
const dbo = require("../server");

const url =
  "mongodb+srv://admin:admin@cluster0.fxsip.mongodb.net/test?authSource=admin&replicaSet=atlas-jam1ck-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true";

const client = new MongoClient(url);

const cachingTimeMillis = 3600000; // 1 hour in millis

const mHashCache = {};

function sortData(key, data, type) {
  let ordered = {};
  let compareFunction = function(a, b) {
    return data[b][key] - data[a][key];
  };
  if (type === "asc") {
    compareFunction = function(a, b) {
      return data[a][key] - data[b][key];
    }
  }
  Object.keys(data).sort(compareFunction).forEach(function(key) {
    ordered[key] = data[key];
  });
  return ordered;
}

exports.getFromCountry = async (req, res, next) => {
  try {
    const mCountry = req.params.country;
    const mTime = new Date().getTime();
    let sortedTrendings = {};
    if (mHashCache[mCountry] !== undefined && mTime - mHashCache[mCountry].time < cachingTimeMillis) {
      sortedTrendings = mHashCache[mCountry].data;
    } else {
      await client.connect();
      const collection = await client.db("Unicorn").collection("startups");
      const startups = await collection.find({}).toArray();
      const mTrendings = new Object();
      const l = startups.length;

      for (let i = 0; i < l; i++) {
        const mLocation = startups[i].Location;
        if (mLocation.indexOf(mCountry) !== -1) {
          const mIndustry = startups[i].Industry;
          const mFunding = startups[i]['Funding USD'];
          if (mTrendings[mIndustry] === undefined) {
            mTrendings[mIndustry] = {funding: mFunding, startups: 1};
          } else {
            mTrendings[mIndustry].funding += mFunding;
            mTrendings[mIndustry].startups += 1;
          }
        }
      }
      sortedTrendings = sortData('funding', mTrendings, 'des');
      mHashCache[mCountry] = {time: mTime, data: sortedTrendings};
    }
  
    res.status(200).json({
      status: "success",
      data: sortedTrendings,
    });
  } catch (e) {
    console.log(e);
    res.status(500).json({
      status: "failed",
      data: {},
    });
  }
  if (next != undefined) next();
};
