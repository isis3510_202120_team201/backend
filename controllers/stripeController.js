const firebase = require("firebase/app");
const firestore = require("firebase/firestore/lite");
const { default: Stripe } = require("stripe");

const stripe = Stripe(
  "sk_test_51K1zfWCLAaGpJJL2jyjn2DRIglZOFgXYI9fnsZ06jU6WltqmShM8LTXUYybfLY1s4xXslLMCB44vyIVTAFLN14Lo00SbJUi4bv"
);


const firebaseConfig = {
  apiKey: "AIzaSyDI1Pj94zAY2TUorI0hbh7gQXjbInBulAI",
  authDomain: "unicorn-2dca3.firebaseapp.com",
  databaseURL: "https://unicorn-2dca3-default-rtdb.firebaseio.com",
  projectId: "unicorn-2dca3",
  storageBucket: "unicorn-2dca3.appspot.com",
  messagingSenderId: "306316302162",
  appId: "1:306316302162:web:7c4f61b2c3b58464ad8c8b",
  measurementId: "G-SJ7H7KJ3NQ"
};

const app = firebase.initializeApp(firebaseConfig);
const db = firestore.getFirestore(app);

exports.stripePayment = async (req, res, next) => {
  try {
    const {id, startDate, finishDate} = req.body;

    console.log(id,startDate,finishDate);

    const start = new Date(startDate);
    const finish = new Date(finishDate);

    const docRef = firestore.doc(db, "promoted", id);

    let profile = await firestore.getDoc(docRef);

    if (profile.exists() && profile.data().finishDate.toDate().getTime() > start.getTime()) {
      res.status(400).json({message: "you still have paid time"});
    } else {

      const data = {
        startDate: firestore.Timestamp.fromDate(start),
        finishDate: firestore.Timestamp.fromDate(finish),
      };

      await firestore.setDoc(firestore.doc(db, "promoted", id),data);

      const paymentIntent = await stripe.paymentIntents.create({
        amount: 10000,
        currency: "usd",
      });

      res.status(200).json({
        payment: paymentIntent.client_secret,
      });
    }
  }catch(e){
    console.log(e);
    res.status(500).json({
      status: "failed",
      data: {},
    });
  }
};
