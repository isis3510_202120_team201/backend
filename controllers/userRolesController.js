const firebase = require("firebase/app");
const firestore = require("firebase/firestore/lite");

const firebaseConfig = {
  apiKey: "AIzaSyDI1Pj94zAY2TUorI0hbh7gQXjbInBulAI",
  authDomain: "unicorn-2dca3.firebaseapp.com",
  databaseURL: "https://unicorn-2dca3-default-rtdb.firebaseio.com",
  projectId: "unicorn-2dca3",
  storageBucket: "unicorn-2dca3.appspot.com",
  messagingSenderId: "306316302162",
  appId: "1:306316302162:web:7c4f61b2c3b58464ad8c8b",
  measurementId: "G-SJ7H7KJ3NQ"
};

const app = firebase.initializeApp(firebaseConfig);
const db = firestore.getFirestore(app);

exports.getUsersWithTwoRoles = async (req, res, next) => {
  try {
    const users = firestore.collection(db, 'users');
    let mData = await firestore.getDocs(users);
    const mUsers = mData.docs.map(doc => doc.data());
    const l = mUsers.length;
    let bothRoles = 0;
    let oneRole = 0;
    for (let i = 0; i < l; i++) {
        if (mUsers[i].type === "Investor" && mUsers[i].pages != undefined && mUsers[i].pages.length > 0) {
            bothRoles += 1;
        } else {
            oneRole += 1;
        }
    }
    const data = {singleRole: oneRole, bothRoles: bothRoles};
    res.status(200).json(data);
  } catch (e) {
    console.log(e);
    res.status(500).json({
      status: "failed",
      data: {},
    });
  }
  if (next != undefined) next();
};
