const firebase = require("firebase/app");
const firestore = require("firebase/firestore/lite");

const firebaseConfig = {
  apiKey: "AIzaSyDI1Pj94zAY2TUorI0hbh7gQXjbInBulAI",
  authDomain: "unicorn-2dca3.firebaseapp.com",
  databaseURL: "https://unicorn-2dca3-default-rtdb.firebaseio.com",
  projectId: "unicorn-2dca3",
  storageBucket: "unicorn-2dca3.appspot.com",
  messagingSenderId: "306316302162",
  appId: "1:306316302162:web:7c4f61b2c3b58464ad8c8b",
  measurementId: "G-SJ7H7KJ3NQ"
};

const app = firebase.initializeApp(firebaseConfig);
const db = firestore.getFirestore(app);

exports.getInformationRelevance = async (req, res, next) => {
  try {
    const users = firestore.collection(db, 'users');
    const mData = await firestore.getDocs(users);
    const mUsers = mData.docs.map(doc => doc.data());
    const l = mUsers.length;
    const data = {};
    for (let i = 0; i < l; i++) {
        if (mUsers[i].surveyScore != undefined) {
            if (data['score' + mUsers[i].surveyScore] != undefined) {
                data['score' + mUsers[i].surveyScore] += 1;
            } else {
                data['score' + mUsers[i].surveyScore] = 1;
            }
        }
    }
    for (let i = 1; i < 11; i++) {
        if (data['score' + i] == undefined) {
            data['score' + i] = 0;
        }
    }
    res.status(200).json(data);
  } catch (e) {
    console.log(e);
    res.status(500).json({
      status: "failed",
      data: {},
    });
  }
  if (next != undefined) next();
};

exports.getContactFormUsage = async (req, res, next) => {
    try {
        const users = firestore.collection(db, 'users');
        const mData = await firestore.getDocs(users);
        const mUsers = mData.docs.map(doc => doc.data());
        const l = mUsers.length;
        let yes = 0;
        let no = 0;
        for (let i = 0; i < l; i++) {
            if (mUsers[i].numContacts != undefined) {
                if (mUsers[i].numContacts > 0) {
                    yes++;
                } else {
                    no++;
                }
            }
        }
        res.status(200).json({yes: yes, no: no});
    } catch (e) {
      console.log(e);
      res.status(500).json({
        status: "failed",
        data: {},
      });
    }
    if (next != undefined) next();
};

exports.getPeoplePostingMoreThanFiveTimes = async (req, res, next) => {
    try {
        const users = firestore.collection(db, 'users');
        const mData = await firestore.getDocs(users);
        const mUsers = mData.docs.map(doc => doc.data());
        const l = mUsers.length;
        let yes = 0;
        let no = 0;
        for (let i = 0; i < l; i++) {
            if (mUsers[i].numPosts != undefined) {
                if (mUsers[i].numPosts > 5) {
                    yes++;
                } else {
                    no++;
                }
            }
        }
        res.status(200).json({yes: yes, no: no});
    } catch (e) {
      console.log(e);
      res.status(500).json({
        status: "failed",
        data: {},
      });
    }
    if (next != undefined) next();
};

exports.getTypeFinancialFunds = async (req, res, next) => {
    try {
        const pages = firestore.collection(db, 'pages');
        const mData = await firestore.getDocs(pages);
        const mPages = mData.docs.map(doc => doc.data());
        const l = mPages.length;
        const data = {};
        for (let i = 0; i < l; i++) {
            if (mPages[i].preferredFinancial != undefined) {
                if (data[mPages[i].preferredFinancial] != undefined) {
                    data[mPages[i].preferredFinancial] += 1;
                } else {
                    data[mPages[i].preferredFinancial] = 1;
                }
            }
        }
        res.status(200).json(data);
    } catch (e) {
      console.log(e);
      res.status(500).json({
        status: "failed",
        data: {},
      });
    }
    if (next != undefined) next();
};