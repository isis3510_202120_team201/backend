const { MongoClient } = require("mongodb");
const dbo = require("../server");

const url =
  "mongodb+srv://admin:admin@cluster0.fxsip.mongodb.net/test?authSource=admin&replicaSet=atlas-jam1ck-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true";

const client = new MongoClient(url);

const cachingTimeMillis = 3600000; // 1 hour in millis

let mHashCache = {};

exports.getCountries = async (req, res, next) => {
  try {
    const mTime = new Date().getTime();
    let sortedCountries = {};
    if (mHashCache !== undefined && mTime - mHashCache.time < cachingTimeMillis) {
      console.log('CACHE');
      sortedCountries = mHashCache.data;
    } else {
      console.log('DB');
      await client.connect();
      const collection = await client.db("Unicorn").collection("startups");
      const startups = await collection.find({}).toArray();
      const mCountries = {};
      const l = startups.length;

      for (let i = 0; i < l; i++) {
        const mTemp = startups[i].Location.split(',')[2];
        const mLocation = mTemp.trim();
        if (mCountries[mLocation] === undefined) {
          mCountries[mLocation] = 1;
        }
      }
      const mTemp = Object.keys(mCountries);
      mTemp.sort();
      sortedCountries = mTemp;
      mHashCache = {time: mTime, data: sortedCountries};
    }
  
    res.status(200).json({
      status: "success",
      data: sortedCountries,
    });
  } catch (e) {
    console.log(e);
    res.status(500).json({
      status: "failed",
      data: {},
    });
  }
  if (next != undefined) next();
};
