const { MongoClient } = require("mongodb");
const dbo = require("../server");

const url =
  "mongodb+srv://admin:admin@cluster0.fxsip.mongodb.net/test?authSource=admin&replicaSet=atlas-jam1ck-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true";

const client = new MongoClient(url);

exports.getAll = async (req, res, next) => {
  try {
    await client.connect();
    const collection = await client.db("Unicorn").collection("startups");

    const startups = await collection.find({}).toArray();

    res.status(200).json({
      status: "success",
      data: {
        startups,
      },
    });
  } catch (e) {
    res.status(500).json({
      status: "failed",
      data: {},
    });
  }

  next();
};

exports.getAllFromIndustry = async (req, res, next) => {
  try {
    await client.connect();
    const collection = await client.db("Unicorn").collection("startups");
    const startups = await collection.find({ Industry: req.params.industry }).toArray();

    res.status(200).json({
      status: "success",
      data: {
        startups,
      },
    });
  } catch (e) {
    res.status(500).json({
      status: "failed",
      data: {},
    });
  }
};
