const firebase = require("firebase/app");
const firestore = require("firebase/firestore/lite");

const firebaseConfig = {
  apiKey: "AIzaSyDI1Pj94zAY2TUorI0hbh7gQXjbInBulAI",
  authDomain: "unicorn-2dca3.firebaseapp.com",
  databaseURL: "https://unicorn-2dca3-default-rtdb.firebaseio.com",
  projectId: "unicorn-2dca3",
  storageBucket: "unicorn-2dca3.appspot.com",
  messagingSenderId: "306316302162",
  appId: "1:306316302162:web:7c4f61b2c3b58464ad8c8b",
  measurementId: "G-SJ7H7KJ3NQ"
};

const app = firebase.initializeApp(firebaseConfig);
const db = firestore.getFirestore(app);

exports.getViews = async (req, res, next) => {
  try {
    const users = firestore.collection(db, 'users');
    let mData = await firestore.getDocs(users);
    const mUsers = mData.docs.map(doc => doc.data());
    const mUsersId = mData.docs.map(doc => doc.id);
    const promoted = firestore.collection(db, 'promoted');
    mData = await firestore.getDocs(promoted);
    const mPromoted = mData.docs.map(doc => doc.id);
    const l = mUsers.length;
    let normalViews = 0;
    let normalCount = 0;
    let promotedViews = 0;
    let promotedCount = 0;
    for (let i = 0; i < l; i++) {
        if (mUsers[i].views != undefined && mUsersId[i] != undefined) {
            if (mPromoted.indexOf(mUsersId[i]) != -1) {
                promotedViews += mUsers[i].views;
                promotedCount += 1;
            } else {
                normalViews += mUsers[i].views;
                normalCount += 1;
            }
        }
    }
    const promotedViewsPer = promotedCount > 0 ? promotedViews / promotedCount : 0;
    const normalViewsPer = normalCount > 0 ? normalViews / normalCount : 0;
    const mNormalViews = normalViewsPer > 0 ? normalViewsPer : 1;
    let increasePer = ((promotedViewsPer / mNormalViews) - 1) * 100;
    const data = {
        promotedViewsAverage: promotedViewsPer,
        normalViewsAverage: normalViewsPer,
        increase: '' + increasePer + '%'
    };
    res.status(200).json(data);
  } catch (e) {
    console.log(e);
    res.status(500).json({
      status: "failed",
      data: {},
    });
  }
  if (next != undefined) next();
};
