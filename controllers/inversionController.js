const { MongoClient } = require("mongodb");
const dbo = require("../server");

const url =
  "mongodb+srv://admin:admin@cluster0.fxsip.mongodb.net/test?authSource=admin&replicaSet=atlas-jam1ck-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true";

const client = new MongoClient(url);

const cachingTimeMillis = 3600000; // 1 hour in millis

let mHashCache = undefined;

function sortData(key, data, type) {
  let ordered = {};
  let compareFunction = function(a, b) {
    return data[b][key] - data[a][key];
  };
  if (type === "asc") {
    compareFunction = function(a, b) {
      return data[a][key] - data[b][key];
    }
  }
  Object.keys(data).sort(compareFunction).forEach(function(key) {
    ordered[key] = data[key];
  });
  return ordered;
}

exports.getFromIndustry = async (req, res, next) => {
  try {
    const mIndustry = req.params.industry;
    const mTime = new Date().getTime();
    let sortedInversion = {};
    if (mHashCache !== undefined && mTime - mHashCache.time < cachingTimeMillis) {
      sortedInversion = mHashCache.data;
    } else {
      await client.connect();
      const collection = await client.db("Unicorn").collection("startups");
      const startups = await collection.find({}).toArray();
      const mInversion = new Object();
      const l = startups.length;

      for (let i = 0; i < l; i++) {
        const mInd = startups[i].Industry;
        const mFunding = startups[i]['Funding USD'];
        if (mInversion[mInd] === undefined) {
          mInversion[mInd] = {funding: mFunding, startups: 1};
        } else {
          mInversion[mInd].funding += mFunding;
          mInversion[mInd].startups += 1;
        }
      }
      sortedInversion = sortData('funding', mInversion, 'des');
      mHashCache = {time: mTime, data: sortedInversion};
    }
  
    res.status(200).json({
      status: "success",
      data: sortedInversion,
    });
  } catch (e) {
    console.log(e);
    res.status(500).json({
      status: "failed",
      data: {},
    });
  }
  if (next != undefined) next();
};
