const express = require("express");

const trendingController = require("../controllers/trendingController");

const router = express.Router();

router.route("/country/:country").get(trendingController.getFromCountry);

module.exports = router;
