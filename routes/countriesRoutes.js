const express = require("express");

const countryController = require("../controllers/countriesController");

const router = express.Router();

router.route("/all/").get(countryController.getCountries);

module.exports = router;
