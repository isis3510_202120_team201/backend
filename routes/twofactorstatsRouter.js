const express = require("express");

const twofactorController = require("../controllers/twofactorController");
const router = express.Router();

router.route("/usage/").get(twofactorController.getUsage);
router.route("/successfulStats/").get(twofactorController.getSuccessfulStats);

module.exports = router;
