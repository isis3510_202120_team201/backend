const express = require("express");

const startUpController = require("../controllers/startUpsController");

const router = express.Router();

router.route("/all").get(startUpController.getAll);
router.route("/industry/:industry").get(startUpController.getAllFromIndustry);

module.exports = router;
