const express = require("express");

const inversionController = require("../controllers/inversionController");

const router = express.Router();

router.route("/all/").get(inversionController.getFromIndustry);

module.exports = router;
