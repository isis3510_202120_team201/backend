const express = require("express");

const profileviewsController = require("../controllers/profileviewsController");
const router = express.Router();

router.route("/views/").get(profileviewsController.getViews);

module.exports = router;
