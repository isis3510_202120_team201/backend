const express = require("express");

const userRolesController = require("../controllers/userRolesController");

const router = express.Router();

router.route("/usersTwoRoles/").get(userRolesController.getUsersWithTwoRoles);

module.exports = router;
