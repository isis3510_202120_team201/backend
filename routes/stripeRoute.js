const express = require("express");
const stripeController = require("../controllers/stripeController");

const router = express.Router();

router.route("/generatePayment").post(stripeController.stripePayment);

module.exports = router;
