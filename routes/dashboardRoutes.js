const express = require("express");

const dashboardController = require("../controllers/dashboardController");

const router = express.Router();

router.route("/informationRelevance/").get(dashboardController.getInformationRelevance);
router.route("/contactFormUsage/").get(dashboardController.getContactFormUsage);
router.route("/peoplePostingMoreThanFiveTimes/").get(dashboardController.getPeoplePostingMoreThanFiveTimes);
router.route("/typeFinancialFunds/").get(dashboardController.getTypeFinancialFunds);


module.exports = router;
